# pgbouncer

* Rebuild from upstream to fix specifically:
  * use libc instead of evdns (which causes intermittent DNS lookup failures within CERN)
  * fix systemd restart (use SIGHUP instead of the internal reload functionality)
  * fix logrotation
